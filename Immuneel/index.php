<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Immuneel</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row logo-nav">
        <div class="col-12 col-md-12   ">
        <div class="offset-md-5">
        <img src="img/immuneel logo-15.jpg" class="img-fluid"  alt=""/> </div>
           
        </div>
       
    </div>
    <!-- <nav class="navbar navbar-light" style="background-color: #ffffff00;">
  <a class="navbar-brand " href="#">
    <img src="img/immuneel-logo-20.png"  class="d-inline-block align-top float-none " alt="">
   
  </a>
</nav> -->
    <div class="row mt-2">
      <div class="col-12 col-md-6 col-lg-6 text-center offset-lg-1">
	
	  
        <img src="img/Invite-webpage .jpg" class="img-fluid mt-5" alt=""/> 
      </div>
      <div class="col-12 col-md-6 col-lg-4 ">
            <div class="login">
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" name="email" id="email" required>
                  </div>
                  <div class="input-group">
                    <input type="number" class="form-control" placeholder="Mobile No." aria-label="Mobile No." aria-describedby="basic-addon1" name="mobnum" id="mobnum" maxlength="11" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="location" id="location" required>
                  </div>
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                </form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>