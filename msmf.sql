-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 07:18 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `msmf`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'akshat', 'aj@gmail.com', 'Hi', '2021-01-18 10:15:26', 'msmf', 0, 0),
(2, 'Dr Manish Diwan', 'Sped.birac@gov.in', 'Heartiest congratulations! India\'s Biotech R&D must surge. Immuneel is an example in this direction. ', '2021-01-18 12:14:22', 'msmf', 0, 0),
(3, 'Manohar', 'manohar@stempeutics.com', 'When we say affordable therapy - how affordable it is? What will be the cost to the patient? ', '2021-01-18 12:18:31', 'msmf', 0, 0),
(4, 'Manohar', 'manohar@stempeutics.com', 'When the therapy will be available for treatment? How long the clinical trial will go? ', '2021-01-18 12:19:48', 'msmf', 0, 0),
(5, 'Shirish Belapure', 'sgbelapure@gmail.com', 'Congratulations all for this great first step. all the best for future\r\n', '2021-01-18 12:20:53', 'msmf', 0, 0),
(6, 'Hart Stefan', 'shart16@its.jnj.com', 'Congratulations !!', '2021-01-18 12:23:05', 'msmf', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `mobile_num` varchar(15) NOT NULL,
  `location` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `mobile_num`, `location`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(3, 'Arun Patre', 'arun.patre@gmail.com', '09880891675', 'Bangalore', NULL, NULL, '2020-12-13 16:45:12', '2020-12-13 21:45:26', '2020-12-13 21:45:56', 0, 'msmf'),
(4, 'Madhavaraj V Sirsi', 'mvsirsi@gmail.com', '9845006057', 'Bengaluru', NULL, NULL, '2020-12-13 16:49:33', '2020-12-13 16:49:33', '2020-12-13 16:50:03', 0, 'msmf'),
(5, 'Salai Jeyaseelan ', 'jeyaseelan@ms-mf.org', '9819912392', 'Bangalore', NULL, NULL, '2020-12-13 16:53:06', '2020-12-13 16:53:06', '2020-12-13 16:53:36', 0, 'msmf'),
(6, 'Sujatha ', 'sujatha@coact.co.in', '984556370', 'Bangalore ', NULL, NULL, '2020-12-14 09:44:04', '2020-12-14 09:44:04', '2020-12-14 09:44:34', 0, 'msmf'),
(7, 'Dr.Arjun.K.R', 'drarjunkr22@gmail.com', '9686862263', 'Bangalore', NULL, NULL, '2020-12-14 10:22:23', '2020-12-15 13:10:28', '2020-12-15 13:56:55', 0, 'msmf'),
(8, 'Akshat Jharia', 'akshatjharia@gmail.com', '07204420017', 'Bangalore', NULL, NULL, '2020-12-14 10:33:00', '2021-01-13 17:05:39', '2021-01-13 17:32:43', 0, 'msmf'),
(9, 'PRIYAMVADA MATHUR', 'priyam.creative@gmail.com', '07023566224', 'New Delhi', NULL, NULL, '2020-12-14 11:12:58', '2020-12-14 11:12:58', '2020-12-14 11:14:30', 0, 'msmf'),
(10, 'Rajlakshmi Borthakur', 'raji@teblux.com', '09886036987', 'Bengaluru (Bangalore) Urban', NULL, NULL, '2020-12-14 11:27:50', '2020-12-15 15:20:25', '2020-12-15 15:20:55', 0, 'msmf'),
(11, 'JAYAVANTH KAMATH', 'jay@icaltech.com', '9845053196', 'BANGALORE', NULL, NULL, '2020-12-14 11:28:13', '2020-12-15 14:02:39', '2020-12-15 14:26:40', 0, 'msmf'),
(12, 'Abhijit Sengupta', 'abhijitsg2004calcutta@gmail.com', '08017648297', 'Kolkata', NULL, NULL, '2020-12-14 11:29:26', '2020-12-14 11:29:26', '2020-12-14 11:29:56', 0, 'msmf'),
(13, 'Shrinivas Savale', 'aic@lmcp.ac.in', '09979052529', 'AHMEDABAD', NULL, NULL, '2020-12-14 11:30:36', '2020-12-15 13:04:00', '2020-12-15 14:09:32', 0, 'msmf'),
(14, 'Siddharth Nair', 'sid@xfinito.com', '9916485848', 'Bangalore', NULL, NULL, '2020-12-14 11:31:10', '2020-12-15 13:12:52', '2020-12-15 13:59:22', 0, 'msmf'),
(15, 'Sridhar Alavalapati', 'alavalsri@gmail.com', '7382373970', 'Narayan Hrudayalaya Basement', NULL, NULL, '2020-12-14 11:34:29', '2020-12-14 11:34:29', '2020-12-14 11:41:25', 0, 'msmf'),
(16, 'Rachna Dave', 'e.coli@microgo.in', '9445980742', 'Chennai', NULL, NULL, '2020-12-14 11:35:49', '2020-12-15 13:11:25', '2020-12-15 13:56:21', 0, 'msmf'),
(17, 'Arshad Moideen Koya ', 'arshadkoya@gmail.com', '8593007647', 'Calicut ', NULL, NULL, '2020-12-14 11:41:40', '2020-12-14 11:41:40', '2020-12-14 11:41:55', 0, 'msmf'),
(18, 'PRAVEEN VADLAMUDI', 'praveen@vidmed.in', '9849012835', 'Hyderabad', NULL, NULL, '2020-12-14 11:49:09', '2020-12-14 11:49:09', '2020-12-14 11:50:39', 0, 'msmf'),
(19, 'PAWAN SHILWANT', 'pawan@coact.co.in', '09545329222', 'Bangalore', NULL, NULL, '2020-12-14 11:49:44', '2020-12-14 11:49:44', '2020-12-14 12:04:15', 0, 'msmf'),
(20, 'Navaneeth', 'kmakaram@yahoo.com', '9445481388', 'Chennai', NULL, NULL, '2020-12-14 11:51:48', '2020-12-14 14:08:04', '2020-12-14 14:08:34', 0, 'msmf'),
(21, 'Dr. Prakash', 'drprakashresearch@gmail.com', '9597888907', 'Coimbatore', NULL, NULL, '2020-12-14 11:58:51', '2020-12-14 11:58:51', '2020-12-14 11:59:52', 0, 'msmf'),
(22, 'Sunita', 'sunita@earthensymphony.com', '7760011598', 'Bangalore', NULL, NULL, '2020-12-14 12:12:52', '2020-12-15 13:06:44', '2020-12-15 13:39:15', 0, 'msmf'),
(23, 'Dr.Sivakumar Muthusamy', 'drsiva@ariviya.com', '9751313534', 'Pattukkottai', NULL, NULL, '2020-12-14 12:17:41', '2020-12-14 12:17:41', '2020-12-14 12:18:11', 0, 'msmf'),
(24, 'Dr. Gandhiya Vendhan S', 'GANDHIYAVENDHAN@YAHOO.COM', '08508858098', 'Coimbatore', NULL, NULL, '2020-12-14 12:23:50', '2020-12-25 09:51:13', '2020-12-25 09:51:43', 0, 'msmf'),
(25, 'atul kherde', 'atul@sushrutdesigns.com', '9975061404', 'PUNE', NULL, NULL, '2020-12-14 12:31:20', '2020-12-14 12:31:20', '2020-12-14 12:33:32', 0, 'msmf'),
(26, 'prakhar jain', 'prakhar@microxlabs.com', '97977997977', 'Bangalore', NULL, NULL, '2020-12-14 12:45:37', '2020-12-15 12:53:59', '2020-12-15 13:57:04', 0, 'msmf'),
(27, 'praneet Kumar', 'praneet.kr@gmail.com', '9810155427', 'New Delhi', NULL, NULL, '2020-12-14 13:04:15', '2020-12-15 13:13:47', '2020-12-15 13:57:39', 0, 'msmf'),
(28, 'Manoj ', 'manoj@nemo.care', '9902908472', 'Bangalore', NULL, NULL, '2020-12-14 13:05:21', '2020-12-15 13:03:28', '2020-12-15 14:02:00', 0, 'msmf'),
(29, 'R. Sridevi', 'sridevirk74@gmail.com', '8754760908', 'Rajapalayam', NULL, NULL, '2020-12-14 13:12:23', '2020-12-15 14:18:44', '2020-12-15 14:19:18', 0, 'msmf'),
(30, 'Joiston Pereira', 'joiston.pereira@fcdo.gov.uk', '8971079602', 'Bangalore', NULL, NULL, '2020-12-14 13:44:14', '2020-12-15 13:00:43', '2020-12-15 13:23:43', 0, 'msmf'),
(31, 'DEVJIT GUPTA', 'gupta.devjit@gmail.com', '9822326854', 'Pune', NULL, NULL, '2020-12-14 14:10:38', '2020-12-14 14:10:38', '2020-12-14 14:11:08', 0, 'msmf'),
(32, 'Abhijit Nair', 'abhijit.nair@adroitecinfo.com', '8600625216', 'Bangalore', NULL, NULL, '2020-12-14 14:11:31', '2020-12-14 14:11:31', '2020-12-14 14:14:32', 0, 'msmf'),
(33, 'KannanPS', 'tbi.kongu@gmail.com', '9943927275', 'Erode', NULL, NULL, '2020-12-14 14:12:51', '2020-12-15 13:37:33', '2020-12-15 13:44:04', 0, 'msmf'),
(34, 'C.Annamalai', 'annamalaimoopanar@gmail.com', '9600922564', 'Rajapalayam ', NULL, NULL, '2020-12-14 14:57:10', '2020-12-14 14:57:10', '2020-12-14 14:57:40', 0, 'msmf'),
(35, 'Shweta Ramamoorthy ', 'shweta@anthillventures.com', '9886722302', 'Bangalore ', NULL, NULL, '2020-12-14 15:00:00', '2020-12-14 15:00:00', '2020-12-14 15:00:07', 0, 'msmf'),
(36, 'Ina Thakur', 'inathakur.startup@gmail.com', '8595333542', 'Noida', NULL, NULL, '2020-12-14 15:19:28', '2020-12-14 15:19:28', '2020-12-14 15:19:58', 0, 'msmf'),
(37, 'Nitin Sisodia', '99.nitin@gmail.com', '09899725208', 'Bangalore', NULL, NULL, '2020-12-14 16:26:18', '2020-12-14 16:26:18', '2020-12-14 16:26:48', 0, 'msmf'),
(38, 'Ashutosh Pandey', 'anshuashutosh24@gmail.com', '9415431931', 'Indis', NULL, NULL, '2020-12-14 17:22:21', '2020-12-14 17:22:21', '2020-12-14 17:22:51', 0, 'msmf'),
(39, 'Dr Jayanth Kesave ', 'jayanth.kesave@gmail.com', '09880265872', 'Bangalore ', NULL, NULL, '2020-12-14 17:39:38', '2020-12-14 17:39:38', '2020-12-14 17:41:38', 0, 'msmf'),
(40, 'Debraj Dasgupta', 'debraj.dasgupta@biocon.com', '90822856', 'Singapore', NULL, NULL, '2020-12-14 18:07:39', '2020-12-14 18:07:39', '2020-12-14 18:08:09', 0, 'msmf'),
(41, 'iyman amin', 'iymanamin619z@gmail.com', '6005160145', 'jammu and kashmir', NULL, NULL, '2020-12-14 18:31:41', '2020-12-14 18:31:41', '2020-12-14 18:32:11', 0, 'msmf'),
(42, 'NIRAV NARENDRABHAI PARMAR', 'krishnaparmar183885@gmail.com', '08401924014', 'VADODARA', NULL, NULL, '2020-12-14 18:33:03', '2020-12-14 18:33:03', '2020-12-14 18:33:33', 0, 'msmf'),
(43, 'Dibita Mandal ', 'dibita1234@gmail.com', '8097701647', 'Mumbai', NULL, NULL, '2020-12-14 19:07:05', '2020-12-14 19:07:05', '2020-12-14 19:07:35', 0, 'msmf'),
(44, 'SALAI JEYASEELAN', 'canyoudroid@gmail.com', '08130611081', 'Rajapalayam', NULL, NULL, '2020-12-14 19:21:41', '2020-12-15 13:40:14', '2020-12-15 14:19:56', 0, 'msmf'),
(45, 'Melvin George', 'melvin.george@biocon.com', '9769197477', 'Bengaluru', NULL, NULL, '2020-12-14 19:21:58', '2021-01-13 13:23:34', '2021-01-13 13:52:34', 0, 'msmf'),
(46, 'ANKUR JAIN', 'ANKURJAIN78@GMAIL.COM', '9810652222', 'GHAZIABAD ', NULL, NULL, '2020-12-14 19:40:07', '2020-12-14 19:40:07', '2020-12-14 19:40:37', 0, 'msmf'),
(47, 'Dr. Haitham Lateef Abdulhadi', 'haitham_bio@yahoo.com', '00964701733', 'Anbar', NULL, NULL, '2020-12-14 20:12:15', '2020-12-14 20:12:15', '2020-12-14 20:12:45', 0, 'msmf'),
(48, 'Abhilesh', 'abhilesh@ayurythm.com', '9449089520', 'Bangalore', NULL, NULL, '2020-12-14 21:20:31', '2020-12-14 21:20:31', '2020-12-14 21:21:01', 0, 'msmf'),
(49, 'Dr. HARISHCHANDER ANANDARAM', 'harishchandera@skasc.ac.in', '09940066227', 'INDIA', NULL, NULL, '2020-12-14 21:21:39', '2020-12-14 21:21:39', '2020-12-14 21:22:09', 0, 'msmf'),
(50, 'Vikas Katoch', 'vikas.katoch@narayanahealth.org', '7018879650', 'Bengaluru', NULL, NULL, '2020-12-14 21:44:18', '2020-12-14 21:44:18', '2020-12-14 21:44:48', 0, 'msmf'),
(51, 'Dr. Lavanya Kiran', 'drlavanya_kiran@yahoo.co.in', '09845689827', 'Bangalore', NULL, NULL, '2020-12-14 22:03:58', '2020-12-14 22:03:58', '2020-12-14 22:04:02', 0, 'msmf'),
(52, 'Dr Kanimozhi Jeyaram', 'kanimozhi.j@klu.ac.in', '8301075390', 'Tamilnadu', NULL, NULL, '2020-12-14 22:16:35', '2020-12-14 22:16:35', '2020-12-15 00:20:05', 0, 'msmf'),
(53, 'Venkatesh Vadde', 'vvadde@sensio-ai.in', '09945715725', 'Bangalore', NULL, NULL, '2020-12-14 22:19:07', '2020-12-15 13:01:32', '2020-12-15 13:51:33', 0, 'msmf'),
(54, 'JIMELLIOT CHRISTOPHERJAMES', 'jimelliot.c@gmail.com', '09486936968', 'Krishnankoil, Tamil Nadu', NULL, NULL, '2020-12-14 22:54:55', '2020-12-14 22:54:55', '2020-12-14 22:55:09', 0, 'msmf'),
(55, 'Shrihari', 'drshrihariomr@gmail.com', '7892121331', 'Bengaluru', NULL, NULL, '2020-12-15 01:26:18', '2020-12-15 13:41:19', '2020-12-15 14:03:47', 0, 'msmf'),
(56, 'Swathi Sri', 'swathisri9899@gmail.com', '6381987849', 'Chennai', NULL, NULL, '2020-12-15 07:26:08', '2020-12-15 07:26:08', '2020-12-15 07:27:09', 0, 'msmf'),
(57, 'Aron Jo', 'aron_jo@pixeldp.com', '00', 'South Korea, Seoul', NULL, NULL, '2020-12-15 08:50:46', '2020-12-15 08:50:46', '2020-12-15 08:51:16', 0, 'msmf'),
(58, 'Ananthapadmanabhan UK', 'ananth.uk@tenxhealthtech.com', '19597033733', 'Chenai', NULL, NULL, '2020-12-15 08:53:15', '2020-12-15 08:53:15', '2020-12-15 09:47:35', 0, 'msmf'),
(59, 'N. RAJESH', 'nambi.rajesh.rajesh@gmail.com', '09014046537', 'Kurnool', NULL, NULL, '2020-12-15 10:02:55', '2020-12-15 10:02:55', '2020-12-15 10:05:26', 0, 'msmf'),
(60, 'Smita Singh', 'smitasvm12@gmail.com', '6388367817', 'Banglore ', NULL, NULL, '2020-12-15 10:11:56', '2020-12-15 14:58:40', '2020-12-15 14:59:10', 0, 'msmf'),
(61, 'Amritha Kailesh', 'amrithakailesh@gmail.com', '07022292115', 'Bangalore', NULL, NULL, '2020-12-15 10:34:26', '2020-12-15 13:10:43', '2020-12-15 14:35:59', 0, 'msmf'),
(62, 'Triloki Keshri', 'triloki@bempu.com', '07676808957', 'Bangalore', NULL, NULL, '2020-12-15 10:38:07', '2020-12-15 13:19:01', '2020-12-15 14:09:59', 0, 'msmf'),
(63, 'Reynold Roy', 'royreynoldexplorer@gmail.com', '09611518064', 'Kokrajhar', NULL, NULL, '2020-12-15 10:38:19', '2020-12-15 10:38:19', '2020-12-15 10:38:30', 0, 'msmf'),
(64, 'yathindra, N.', 'yathindra@ibab.ac.in', '9845507031', 'Electronics city, Bengaluru', NULL, NULL, '2020-12-15 11:02:42', '2020-12-15 12:53:56', '2020-12-15 13:58:13', 0, 'msmf'),
(65, 'Suhela Kapoor', 'suhela.kapoor@gmail.com', '9871244289', 'Delhi', NULL, NULL, '2020-12-15 11:08:11', '2020-12-15 11:08:11', '2020-12-15 11:09:12', 0, 'msmf'),
(66, 'Naresh Trehan', 'naresh@tfccasia.com', '9718460727', 'newdelhi', NULL, NULL, '2020-12-15 11:12:40', '2020-12-15 11:12:40', '2020-12-15 11:35:11', 0, 'msmf'),
(67, 'Vikas manikrao shukre', 'vikasshukre@gmail.com', '07020066239', 'aurangabad', NULL, NULL, '2020-12-15 11:29:28', '2020-12-15 11:29:28', '2020-12-15 12:22:59', 0, 'msmf'),
(68, 'Merin Philip', 'merinphilip8@gmail.com', '8851353468', 'Faridabad ', NULL, NULL, '2020-12-15 11:38:07', '2020-12-15 11:38:07', '2020-12-15 11:39:08', 0, 'msmf'),
(69, 'Srikanth ', 'kaitojusrikanth@gmail.com', '7899144433', 'Bangalore ', NULL, NULL, '2020-12-15 11:58:15', '2020-12-15 13:08:37', '2020-12-15 13:29:05', 0, 'msmf'),
(70, 'Hari', 'Hk7733@gmail.com', '9886273657', 'India ', NULL, NULL, '2020-12-15 11:58:29', '2020-12-15 11:58:29', '2020-12-15 11:58:59', 0, 'msmf'),
(71, 'Dr Bhuvnesh Shrivastava', 'mii01.birac@nic.in', '09711581760', 'New Delhi', NULL, NULL, '2020-12-15 12:11:36', '2020-12-15 13:06:33', '2020-12-15 14:15:03', 0, 'msmf'),
(72, 'tp53', 'spiff007@gmail.com', '0', 'blr', NULL, NULL, '2020-12-15 12:22:23', '2020-12-15 13:05:46', '2020-12-15 13:42:16', 0, 'msmf'),
(73, 'Deepak Thandassery', 'Deepak.balram@gmail.com', '9845169424', 'Bangalore', NULL, NULL, '2020-12-15 12:27:03', '2020-12-15 12:27:03', '2020-12-15 13:02:53', 0, 'msmf'),
(74, 'Nameeta Shah', 'nameeta.shah@gmail.com', '8884873450', 'Bangalore', NULL, NULL, '2020-12-15 12:39:58', '2020-12-15 12:39:58', '2020-12-15 13:55:25', 0, 'msmf'),
(75, 'Ravi Kiran Manapuram', 'ravikiran@medevplus.com', '9908018822', 'Bangalore', NULL, NULL, '2020-12-15 12:47:54', '2020-12-15 12:47:54', '2020-12-15 15:00:19', 0, 'msmf'),
(76, 'Anushkaa Agrawal', 'anushka618@gmail.com', '09643941005', 'Ghaziabad', NULL, NULL, '2020-12-15 12:49:06', '2020-12-15 15:10:03', '2020-12-15 15:26:58', 0, 'msmf'),
(77, 'Srirang Ramamoorthy', 'sri.rang@yahoo.com', '09902260163', 'Coimbatore', NULL, NULL, '2020-12-15 12:49:49', '2020-12-15 12:49:49', '2020-12-15 12:50:50', 0, 'msmf'),
(78, 'Antpny Aneel Joseph', 'aneelantony@gmail.com', '9854320821', 'mysore', NULL, NULL, '2020-12-15 12:50:01', '2020-12-15 12:50:01', '2020-12-15 12:52:35', 0, 'msmf'),
(79, 'JoAnn', 'joann.ponce@gmail.com', '7760107908', 'Bangalore', NULL, NULL, '2020-12-15 12:50:29', '2020-12-15 13:04:39', '2020-12-15 13:58:11', 0, 'msmf'),
(80, 'Dr K C Chandrasekharan Nair', 'kccnair@gmail.com', '9447111244', 'Trivandrum', NULL, NULL, '2020-12-15 12:51:38', '2020-12-15 13:13:33', '2020-12-15 13:54:43', 0, 'msmf'),
(81, 'Dr. Preeti Nigam Joshi', 'preetijoshi@fastsensediagnostics.com', '7558368126', 'Pune', NULL, NULL, '2020-12-15 12:52:12', '2020-12-15 12:52:12', '2020-12-15 15:42:12', 0, 'msmf'),
(82, 'Ram Kumar Pandian S', 'srkpandian@klu.ac.in', '9003440063', 'Srivilliputhur', NULL, NULL, '2020-12-15 12:53:01', '2020-12-15 12:53:01', '2020-12-15 13:26:02', 0, 'msmf'),
(83, 'Dr Chhaya Chauhan', 'sped4.birac@nic.in', '8588869455', 'Delhi', NULL, NULL, '2020-12-15 12:56:02', '2020-12-15 12:56:02', '2020-12-15 13:57:22', 0, 'msmf'),
(84, 'Utkarsh Mathur ', 'sped3.birac@nic.in', '9873368445', 'New Delhi ', NULL, NULL, '2020-12-15 12:56:03', '2020-12-15 12:56:03', '2020-12-16 16:34:21', 0, 'msmf'),
(85, 'Ms.G.Nadana', 'nadanarajavadivu@klu.ac.in', '8675086352', 'Madurai', NULL, NULL, '2020-12-15 12:56:38', '2020-12-15 13:25:47', '2020-12-15 13:43:47', 0, 'msmf'),
(86, 'Prarthana', 'prarthana.patre@wfglobal.org', '9886028454', 'Bangalore', NULL, NULL, '2020-12-15 12:58:24', '2020-12-15 12:58:24', '2020-12-15 13:18:22', 0, 'msmf'),
(87, 'Apoorva Moon', 'apoorvamoon@gmail.com', '8452020253', 'Mumbai', NULL, NULL, '2020-12-15 12:59:21', '2020-12-15 13:35:54', '2020-12-15 13:46:25', 0, 'msmf'),
(88, 'Rashmi Mabiyan', 'Rashmi.Mabiyan@timesinternet.in', '08800380635', 'NEW DELHI', NULL, NULL, '2020-12-15 13:00:01', '2020-12-15 13:33:14', '2020-12-15 13:37:44', 0, 'msmf'),
(89, 'Prash', 'prash.chopra@gmail.com', '16504824563', 'San Francisco', NULL, NULL, '2020-12-15 13:00:02', '2020-12-15 13:00:02', '2020-12-15 13:57:35', 0, 'msmf'),
(90, 'Geetha jimmy', 'geethajim84@gmail.com', '9895381611', 'Ernakulam', NULL, NULL, '2020-12-15 13:00:09', '2020-12-15 13:33:19', '2020-12-15 13:46:51', 0, 'msmf'),
(91, 'Srirang Ramamoorthy', 'ikanektrnd@gmail.com', '09902260163', 'Coimbatore', NULL, NULL, '2020-12-15 13:00:50', '2020-12-15 13:44:03', '2020-12-15 13:58:33', 0, 'msmf'),
(92, 'Ramakrishnan Ravi', 'ramakrishnanravi1991@gmail.com', '9659633163', 'Coimbatore', NULL, NULL, '2020-12-15 13:02:30', '2020-12-15 13:02:30', '2020-12-15 13:03:35', 0, 'msmf'),
(93, 'Priya Anant', 'priya@lifecircle.in', '08008417989', 'hyderabad', NULL, NULL, '2020-12-15 13:02:31', '2020-12-15 13:02:31', '2020-12-15 13:09:28', 0, 'msmf'),
(94, 'Vivek Saxena', 'vivek.saxena@thegain.in', '8618137835', 'Bangalore', NULL, NULL, '2020-12-15 13:02:48', '2020-12-15 13:02:48', '2020-12-15 14:07:36', 0, 'msmf'),
(95, 'Sarvesh', 'knowlabs@coin-medix.com', '7975589682', 'Panaji Goa', NULL, NULL, '2020-12-15 13:03:08', '2020-12-15 13:03:08', '2020-12-15 13:09:17', 0, 'msmf'),
(96, 'Taslimarif Saiyed', 'taslim@ccamp.res.in', '8067185100', 'Bangalore', NULL, NULL, '2020-12-15 13:03:29', '2020-12-15 13:03:29', '2020-12-15 13:17:37', 0, 'msmf'),
(97, 'Abhishek Ramkrishna', 'abhishek.ramkrishna@biocon.com', '9892522880', 'Mumbai', NULL, NULL, '2020-12-15 13:04:19', '2021-01-18 11:56:39', '2021-01-18 12:26:10', 0, 'msmf'),
(98, 'Nandita Vijay ', 'nandita@saffronmedia.in', '9845034642', 'Bangalore ', NULL, NULL, '2020-12-15 13:05:00', '2021-01-13 15:15:49', '2021-01-13 15:15:56', 0, 'msmf'),
(99, 'Ganapathy', 'ganapathy.v@gmail.com', '8041677099', 'Bangalore', NULL, NULL, '2020-12-15 13:05:16', '2020-12-15 13:05:16', '2020-12-15 13:44:35', 0, 'msmf'),
(100, 'Swaminathan Kumar', 'swami.kumar93@gmail.com', '9971986911', 'delhi', NULL, NULL, '2020-12-15 13:05:34', '2020-12-15 13:05:34', '2020-12-15 14:38:38', 0, 'msmf'),
(101, 'Pratham', 'pratham.bhat@thegain.in', '9427256345', 'Bengaluru', NULL, NULL, '2020-12-15 13:06:24', '2020-12-15 13:06:24', '2020-12-15 13:08:25', 0, 'msmf'),
(102, 'Sudhir S R', 'sudhir.sr@thegain.in', '9900197390', 'Bangalore', NULL, NULL, '2020-12-15 13:06:45', '2020-12-15 13:06:45', '2020-12-15 13:42:58', 0, 'msmf'),
(103, 'SAFEENA KULSUM', 'safeena.kulsum@ms-mf.org', '9538527911', 'msmf', NULL, NULL, '2020-12-15 13:06:50', '2020-12-15 13:06:50', '2020-12-15 13:57:34', 0, 'msmf'),
(104, 'Dr Vini Kaila', 'drvinikaila@gmail.com', '8332980120', 'BENGALURU', NULL, NULL, '2020-12-15 13:07:00', '2020-12-15 13:07:49', '2020-12-15 13:58:37', 0, 'msmf'),
(105, 'Anusha Kamath', 'anusha@villgro.org', '9167129093', 'Bengaluru', NULL, NULL, '2020-12-15 13:08:35', '2020-12-15 13:08:35', '2020-12-15 13:16:15', 0, 'msmf'),
(106, 'Poonam Bishnani', 'poonambishnani@gmail.com', '7498119619', 'Delhi', NULL, NULL, '2020-12-15 13:09:29', '2020-12-15 13:09:29', '2020-12-15 13:15:29', 0, 'msmf'),
(107, 'BIJU JACOB', 'bijujacobk@gmail.com', '09632725257', 'Bangalore', NULL, NULL, '2020-12-15 13:09:43', '2020-12-15 13:43:34', '2020-12-15 14:09:05', 0, 'msmf'),
(108, 'Ayan Pramanik', 'ayan.pramanik@timesgroup.com', '9730240099', 'Bangalore', NULL, NULL, '2020-12-15 13:11:46', '2020-12-15 13:21:22', '2020-12-15 14:32:20', 0, 'msmf'),
(109, 'Deepthi Subbaraya', 'deepthi@microgo.in', '09566093523', 'Chennai', NULL, NULL, '2020-12-15 13:12:33', '2020-12-15 13:45:19', '2020-12-15 14:00:49', 0, 'msmf'),
(110, 'Jimmy Thomas', 'jimmynj@hotmail.com', '9745401384', 'Ernakulam', NULL, NULL, '2020-12-15 13:13:54', '2020-12-15 13:44:07', '2020-12-15 13:58:56', 0, 'msmf'),
(111, 'S Paul', 'suman.paul01@narayanahealth.org', '9742716644', 'BANGALORE', NULL, NULL, '2020-12-15 13:16:18', '2020-12-15 13:16:18', '2020-12-15 13:55:49', 0, 'msmf'),
(112, 'Parvathi', 'parvathi.c@regrow.in', '7045938934', 'Mumbai', NULL, NULL, '2020-12-15 13:16:20', '2020-12-15 13:16:20', '2020-12-15 13:20:20', 0, 'msmf'),
(113, 'AVIK DAS', 'dasavik324@gmail.com', '7406463821', 'BENGALURU', NULL, NULL, '2020-12-15 13:17:09', '2020-12-15 13:17:09', '2020-12-15 13:57:39', 0, 'msmf'),
(114, 'Pooja', 'pooja@coact.co.in', '9768161921', 'Mumbai', NULL, NULL, '2020-12-15 13:18:20', '2021-01-18 11:40:16', '2021-01-18 12:00:06', 0, 'msmf'),
(115, 'viraj', 'viraj@coact.co.in', '9765875731', 'Pune', NULL, NULL, '2020-12-15 13:21:02', '2021-01-18 11:45:14', '2021-01-18 12:53:44', 0, 'msmf'),
(116, 'Nasir Ashraf', 'nasir_ashraf@hotmail.com', '9596582963', 'Bangalore', NULL, NULL, '2020-12-15 13:24:01', '2020-12-15 13:24:01', '2020-12-15 14:01:31', 0, 'msmf'),
(117, 'Vaibhav Mehta', 'vaibhav.mehta@outlook.com.au', '447523952', 'Melbourne ', NULL, NULL, '2020-12-15 13:25:14', '2020-12-15 13:25:14', '2020-12-15 13:38:45', 0, 'msmf'),
(118, 'Sarvesh', 'Collab.coin24@gmail.com', '7', 'Panaji', NULL, NULL, '2020-12-15 13:25:28', '2020-12-15 13:34:44', '2020-12-15 14:00:44', 0, 'msmf'),
(119, 'Vicky Roy', 'vicky88.roy@gmail.com', '9538382220', 'Bangalore', NULL, NULL, '2020-12-15 13:29:36', '2020-12-15 13:29:36', '2020-12-15 13:53:16', 0, 'msmf'),
(120, 'nidhi', 'nidhi@axilor.com', '4444222244', 'Bangalore', NULL, NULL, '2020-12-15 13:30:42', '2020-12-15 13:30:42', '2020-12-15 13:56:13', 0, 'msmf'),
(121, 'Hemali', 'hbsangani@gmail.com', '9819681103', 'Mumbai', NULL, NULL, '2020-12-15 13:35:12', '2020-12-15 13:35:12', '2020-12-15 13:42:45', 0, 'msmf'),
(122, 'Johnson ', 'jjohnsonsanthosh@gmail.com', '9739346482', 'Bangalore ', NULL, NULL, '2020-12-15 13:36:28', '2020-12-15 13:36:28', '2020-12-15 13:44:58', 0, 'msmf'),
(123, 'Sudhanshu Chaturvedi', 'sudhanshu.chaturvedi@altem.com', '9972682511', 'Bangalore', NULL, NULL, '2020-12-15 13:40:47', '2020-12-15 13:40:47', '2020-12-15 14:15:36', 0, 'msmf'),
(124, 'Sunil Bhat', 'sunilbhat_9@hotmail.com', '9538965968', 'NH Bangalore ', NULL, NULL, '2020-12-15 13:41:49', '2020-12-15 13:41:49', '2020-12-15 13:58:19', 0, 'msmf'),
(125, 'Kapil', 'kapil.kumawat.dr@narayanahealth.org', '9694094523', 'bangalore', NULL, NULL, '2020-12-15 13:43:14', '2020-12-15 13:43:14', '2020-12-15 13:58:45', 0, 'msmf'),
(126, 'Salai arulthai', 'arulthai.ksr@gmail.com', '7708878926', 'Thiruthani', NULL, NULL, '2020-12-15 13:43:15', '2020-12-15 13:44:01', '2020-12-15 13:59:32', 0, 'msmf'),
(127, 'John', 'john.vincent@narayanahealth.org', '9535054777', 'Bangalore', NULL, NULL, '2020-12-15 13:44:11', '2020-12-15 13:44:11', '2020-12-15 15:19:42', 0, 'msmf'),
(128, 'Jayant', 'jayant.jbhargav.dr@narayanahealth.org', '9886153307', 'Bangalore', NULL, NULL, '2020-12-15 13:45:43', '2020-12-15 13:45:43', '2020-12-15 14:19:55', 0, 'msmf'),
(129, 'ratan gupta', 'ratang2015@gmail.com', '09845138504', 'Bengaluru', NULL, NULL, '2020-12-15 13:46:57', '2020-12-15 13:46:57', '2020-12-15 13:56:57', 0, 'msmf'),
(130, 'anilkumar sapare', 'aksapare@hotmail.com', '09880410427', 'bengaluru', NULL, NULL, '2020-12-15 13:47:49', '2020-12-15 13:47:49', '2020-12-15 14:15:14', 0, 'msmf'),
(131, 'Dr saroj', 'Dr.sarojsheoran@gmail.com', '09999404067', 'WEST DELHI', NULL, NULL, '2020-12-15 13:50:00', '2020-12-15 13:50:00', '2020-12-15 14:11:02', 0, 'msmf'),
(132, 'Rupa Rao', 'rupa@healthelife.in', '9980036375', 'Bangalore', NULL, NULL, '2020-12-15 13:53:34', '2020-12-15 13:53:34', '2020-12-15 13:59:33', 0, 'msmf'),
(133, 'Joel S', 'joesta@um.dk', '9620355866', 'Bangalore', NULL, NULL, '2020-12-15 13:55:12', '2020-12-15 13:55:12', '2020-12-15 14:01:26', 0, 'msmf'),
(134, 'Dr Sunil Kumar V', 'drvsunilkumarcta@gmail.com', '09731522994', 'BangaloreÂ [Bangalore]', NULL, NULL, '2020-12-15 13:57:46', '2020-12-15 13:57:46', '2020-12-15 13:58:46', 0, 'msmf'),
(135, 'Raju Gupta', 'rajugupta2052@gmail.com', '8778715344', 'NH', NULL, NULL, '2020-12-15 14:05:14', '2020-12-15 14:05:14', '2020-12-15 14:05:44', 0, 'msmf'),
(136, 'Omprakash Gupta', 'opguptamails@gmail.com', '9849095092', 'Hyderabad india', NULL, NULL, '2020-12-15 14:14:05', '2020-12-15 14:14:05', '2020-12-15 14:14:35', 0, 'msmf'),
(137, 'Rohit', 'lalwani.rohit@gmail.com', '9850509366', 'Pune', NULL, NULL, '2020-12-15 14:19:41', '2020-12-15 14:19:41', '2020-12-15 14:41:41', 0, 'msmf'),
(138, 'Dr Neha Jain', 'drnehalunkad@gmail.com', '9844971654', 'Bangalore', NULL, NULL, '2020-12-15 14:30:43', '2020-12-15 14:30:43', '2020-12-15 14:31:13', 0, 'msmf'),
(139, 'Gopal Chitta', 'gopalchitta@instahealusa.com', '9940655992', 'Chennai', NULL, NULL, '2020-12-15 14:36:17', '2020-12-15 14:36:17', '2020-12-15 14:36:30', 0, 'msmf'),
(140, 'Siddharth Jain', 'siddharth@symbiorph.in', '09998038576', 'Ahmedabad', NULL, NULL, '2020-12-15 14:58:40', '2020-12-15 14:58:40', '2020-12-15 14:59:04', 0, 'msmf'),
(141, 'Dr. chandrashekara GK', 'gk.chandu7486@gmail.com', '09482840737', 'bangalore', NULL, NULL, '2020-12-15 14:59:46', '2020-12-15 14:59:46', '2020-12-15 15:00:16', 0, 'msmf'),
(142, 'Gavin', 'gavin2001chang@gmail.com', '9008020293', 'Bangalore', NULL, NULL, '2020-12-15 15:18:51', '2020-12-15 15:18:51', '2020-12-15 15:24:22', 0, 'msmf'),
(143, 'Bhavna Ambudkar', 'deanalumniniii@dypvp.edu.in', '9890094521', 'Pune', NULL, NULL, '2020-12-15 15:21:34', '2020-12-15 15:21:34', '2020-12-15 15:22:04', 0, 'msmf'),
(144, 'Vishnu', 'vishnukantp123@gmail.com', '8073984552', 'Bangalore', NULL, NULL, '2020-12-15 15:35:08', '2020-12-15 15:35:08', '2020-12-15 15:36:38', 0, 'msmf'),
(145, 'Varad', 'varadk@inductsoftware.com', '9845263959', 'Bangalore ', NULL, NULL, '2020-12-15 17:13:24', '2020-12-15 17:13:24', '2020-12-15 17:15:54', 0, 'msmf'),
(146, 'Archana', 'annjarchana@gmail.com', '8861239821', 'Bangalore', NULL, NULL, '2020-12-15 18:12:51', '2020-12-15 18:12:51', '2020-12-15 18:13:21', 0, 'msmf'),
(147, 'Himshi', 'himshi.bachchas@gmail.com', '9666839015', 'Noida', NULL, NULL, '2020-12-15 19:10:10', '2020-12-15 19:10:10', '2020-12-15 19:11:53', 0, 'msmf'),
(148, 'Mani ', 'manikumar78@gmail.com', '9581689900', 'Chennai ', NULL, NULL, '2020-12-16 06:09:39', '2020-12-16 06:09:39', '2020-12-16 06:10:09', 0, 'msmf'),
(149, 'Basant Kumar', 'Kumarbasant17april@gmail.com', '8240296026', 'Bengaluru', NULL, NULL, '2020-12-16 23:44:44', '2020-12-16 23:44:44', '2020-12-16 23:45:50', 0, 'msmf'),
(150, 'Dr Anusha Sharma PS ', '502343@narayanahealth.org', '7829294922', 'Bangalore ', NULL, NULL, '2020-12-17 10:34:02', '2020-12-17 10:34:02', '2020-12-17 10:34:12', 0, 'msmf'),
(151, 'Pradeep Dubey', 'pradeepsdubey@gmail.com', '07045457236', 'Navi Mumbai', NULL, NULL, '2020-12-20 13:31:24', '2020-12-20 13:31:24', '2020-12-20 13:31:54', 0, 'msmf'),
(152, 'Lavanya C J', 'ju19200@gmail.com', '09840388523', 'Chennai', NULL, NULL, '2020-12-24 15:39:59', '2020-12-24 15:39:59', '2020-12-24 15:44:05', 0, 'msmf'),
(153, 'Preethi. R', 'preethi.rodrigues@immuneel.com', '9972722779', 'Bangalore', NULL, NULL, '2021-01-12 12:39:34', '2021-01-13 15:23:59', '2021-01-13 18:47:29', 0, 'msmf'),
(154, 'Prachi Dubey', 'prachi.dubey@immuneel.com', '9949021161', 'Bangalore', NULL, NULL, '2021-01-12 12:39:56', '2021-01-14 13:45:22', '2021-01-14 14:36:11', 0, 'msmf'),
(155, 'Prachi Dubey', 'prachi.dubey@inmuneel.com', '9949021161', 'Bangalore', NULL, NULL, '2021-01-12 17:48:58', '2021-01-12 17:48:58', '2021-01-12 17:54:10', 0, 'msmf'),
(156, 'neeraj', 'naga@gmail.com', '8788', '6465', NULL, NULL, '2021-01-13 10:10:44', '2021-01-13 13:48:17', '2021-01-13 13:52:14', 0, 'msmf'),
(157, 'neeraj', 'naganeerajereddy9989@gmail.com', '8788', '6465', NULL, NULL, '2021-01-13 11:01:32', '2021-01-13 11:01:32', '2021-01-13 11:02:02', 0, 'msmf'),
(158, 'test', 'test@test.com', '0000000000', 'unknown', NULL, NULL, '2021-01-13 11:50:46', '2021-01-13 11:50:46', '2021-01-13 11:53:43', 0, 'msmf'),
(159, 'Nishanth', 'nishanth@coact.co.in', '08747973536', 'Bangalore', NULL, NULL, '2021-01-13 12:44:26', '2021-01-18 11:53:51', '2021-01-18 11:59:18', 0, 'msmf'),
(160, 'Nitha Paul', 'nitha.paul@immuneel.com', '9886077426', 'Bangalore', NULL, NULL, '2021-01-13 14:26:39', '2021-01-13 14:26:39', '2021-01-13 15:39:11', 0, 'msmf'),
(161, 'Vikas Dandekar', 'vikas.dandekar@timesinternet.in', '9821111490', 'Mumbai', NULL, NULL, '2021-01-13 15:42:29', '2021-01-13 15:42:29', '2021-01-13 15:42:59', 0, 'msmf'),
(162, 'Francis Van Parys', 'francis.vanparys@cytiva.com', '82103153781', 'Seoul, Korea', NULL, NULL, '2021-01-13 16:02:14', '2021-01-18 11:30:04', '2021-01-18 12:24:51', 0, 'msmf'),
(163, 'Shirish Belapure', 'sgbelapure@gmail.com', '09974051999', 'Ahmedabad,INDIA', NULL, NULL, '2021-01-13 16:19:23', '2021-01-18 11:54:58', '2021-01-18 12:29:28', 0, 'msmf'),
(164, 'Narendra Chirmule ', 'chirmule@gmail.com', '9538777099', 'Philadelphia ', NULL, NULL, '2021-01-13 16:24:11', '2021-01-13 16:24:11', '2021-01-13 16:24:41', 0, 'msmf'),
(165, 'Sandeep N Athalye', 'sandeep.athalye@biocon.com', '7338308811', 'BANGALORE', NULL, NULL, '2021-01-13 22:41:05', '2021-01-13 22:41:05', '2021-01-13 22:41:13', 0, 'msmf'),
(166, 'Bala Manian', 'bala@manian.org', '16502839256', 'Pacific Time Zone USA', NULL, NULL, '2021-01-14 00:27:25', '2021-01-14 00:27:25', '2021-01-14 00:27:36', 0, 'msmf'),
(167, 'Manel Juan', 'mjuan@clinic.cat', '657510669', 'Barcelona', NULL, NULL, '2021-01-14 12:38:16', '2021-01-18 13:22:20', '2021-01-18 14:38:33', 0, 'msmf'),
(168, 'Montserrat Cofan', 'mcofan@clinic.cat', '636295015', 'Spain', NULL, NULL, '2021-01-14 12:46:01', '2021-01-18 11:28:32', '2021-01-18 12:47:14', 0, 'msmf'),
(169, 'Dhruv Dayal', 'dhruv.dayal@thermofisher.com', '9873350136', 'New Delhi', NULL, NULL, '2021-01-15 14:36:32', '2021-01-18 11:32:24', '2021-01-18 13:45:25', 0, 'msmf'),
(170, 'RITA  MULHERKAR', 'rmulherkar@gmail.com', '9322225645', 'Kharghar ', NULL, NULL, '2021-01-15 20:11:44', '2021-01-18 11:42:21', '2021-01-18 12:00:24', 0, 'msmf'),
(171, 'BN Manohar', 'manohar@stempeutics.com', '09945630983', 'Bangalore', NULL, NULL, '2021-01-15 21:01:48', '2021-01-18 11:36:39', '2021-01-18 12:25:10', 0, 'msmf'),
(172, 'Thomas Lejolly', 'thomas.lejolly@cytiva.com', '94894187', 'Singapore', NULL, NULL, '2021-01-15 21:10:48', '2021-01-18 12:26:36', '2021-01-18 12:48:06', 0, 'msmf'),
(173, 'Melanie Matheu', 'Melanie@PrellisBio.com', '9496901892', 'San Francisco', NULL, NULL, '2021-01-16 01:30:47', '2021-01-16 01:30:47', '2021-01-16 06:20:18', 0, 'msmf'),
(174, 'SR.Rao', 'vp@sbvu.ac.in', '9818541897', 'Pondicherry', NULL, NULL, '2021-01-16 11:36:53', '2021-01-18 11:27:09', '2021-01-18 12:32:39', 0, 'msmf'),
(175, 'Anushree', 'anushree@cellvec.com', '90180669', 'Singapore', NULL, NULL, '2021-01-16 13:10:30', '2021-01-18 11:29:53', '2021-01-18 12:47:22', 0, 'msmf'),
(176, 'Amit Chopra', 'amit.chopra@thermofisher.com', '09820940690', 'NaviMumbai', NULL, NULL, '2021-01-16 15:44:13', '2021-01-18 11:31:55', '2021-01-18 12:24:27', 0, 'msmf'),
(177, 'akshat', 'aj@gmail.com', '724420', 'BNG', NULL, NULL, '2021-01-17 18:08:26', '2021-01-18 10:15:01', '2021-01-18 13:04:55', 0, 'msmf'),
(178, 'cheng teo', 'teocp@pcc.sg', '90629917', 'singapore', NULL, NULL, '2021-01-18 09:01:33', '2021-01-18 11:35:51', '2021-01-18 12:43:34', 0, 'msmf'),
(179, 'Latha', 'latha.sharma@biocon.com', '9448072233', 'Biocon', NULL, NULL, '2021-01-18 10:48:23', '2021-01-18 11:26:37', '2021-01-18 15:35:00', 0, 'msmf'),
(180, 'Pavan Kumar', 'pavankumar.g.r@thermofisher.com', '9845388832', 'Bengaluru', NULL, NULL, '2021-01-18 11:16:49', '2021-01-18 11:16:49', '2021-01-18 12:43:33', 0, 'msmf'),
(181, 'RANGANATHAN RAGHAVAN', 'ranganathan.raghavan@cytiva.com', '09810013914', 'Chennai', NULL, NULL, '2021-01-18 11:19:12', '2021-01-18 11:19:12', '2021-01-18 12:44:08', 0, 'msmf'),
(182, 'ARSHAD JAMIL', 'arshad.jamil@biocon.com', '09886422322', 'BANGALORE', NULL, NULL, '2021-01-18 11:19:58', '2021-01-18 11:44:25', '2021-01-18 13:15:24', 0, 'msmf'),
(183, 'Radhakrishnan G', 'radhakrishnan.menon@immuneel.com', '9845188682', 'USA', NULL, NULL, '2021-01-18 11:20:41', '2021-01-18 11:20:41', '2021-01-18 12:47:17', 0, 'msmf'),
(184, 'MSMF', 'kanagaraj@ms-mf.org', '9994543672', 'Bangalore', NULL, NULL, '2021-01-18 11:23:35', '2021-01-18 11:23:35', '2021-01-18 12:40:11', 0, 'msmf'),
(185, 'Sunil Bobba', 'sunil.bobba@sunbobbaconsultingservices.com', '9964619111', 'Bengaluru ', NULL, NULL, '2021-01-18 11:23:49', '2021-01-18 11:48:09', '2021-01-18 12:30:02', 0, 'msmf'),
(186, 'Guljit Chaudhri', 'guljit@bioinnovat.com', '9810027227', 'Gurugram', NULL, NULL, '2021-01-18 11:24:38', '2021-01-18 11:33:39', '2021-01-18 12:47:40', 0, 'msmf'),
(187, 'Pankaj Patel', 'pankaj@zyduscadila.com', '09974077700', 'Ahmedbabd', NULL, NULL, '2021-01-18 11:27:55', '2021-01-18 11:27:55', '2021-01-18 12:14:55', 0, 'msmf'),
(188, 'MS', 'minal.shah@immuneel.com', '9372869261', 'Mumbai', NULL, NULL, '2021-01-18 11:28:02', '2021-01-18 11:57:04', '2021-01-18 12:43:01', 0, 'msmf'),
(189, 'Neehar Bhatia', 'bhatianconsulting@gmail.com', '6087706352', 'Santa Clara', NULL, NULL, '2021-01-18 11:28:07', '2021-01-18 11:28:07', '2021-01-18 12:26:03', 0, 'msmf'),
(190, 'Siddhi Kinkar', 'Siddhi.Kinkar@symphonytech.com', '9552573451', 'Pune', NULL, NULL, '2021-01-18 11:28:41', '2021-01-18 11:59:33', '2021-01-18 12:34:29', 0, 'msmf'),
(191, 'Sri Harsha Movva', 'sri.movva@thermofisher.com', '9108432777', 'Bangalore', NULL, NULL, '2021-01-18 11:28:54', '2021-01-18 11:29:43', '2021-01-18 12:47:14', 0, 'msmf'),
(192, 'Hart Stefan', 'shart16@its.jnj.com', '+65 8223483', 'Singapore ', NULL, NULL, '2021-01-18 11:29:07', '2021-01-18 11:29:07', '2021-01-18 12:25:59', 0, 'msmf'),
(193, 'AJAY TYAGI', 'ajaytyagi934@gmail.com', '9717832663', 'New Delhi', NULL, NULL, '2021-01-18 11:29:20', '2021-01-18 11:29:20', '2021-01-18 12:47:50', 0, 'msmf'),
(194, 'Sundaresh', 'sr.sundaresh@gmail.com', '9845028823', 'Bangalore', NULL, NULL, '2021-01-18 11:29:49', '2021-01-18 11:29:49', '2021-01-18 12:28:14', 0, 'msmf'),
(195, 'Chandana', 'shibutulumuke@yahoo.com', '9810273094', 'Delhi', NULL, NULL, '2021-01-18 11:30:20', '2021-01-18 11:30:20', '2021-01-18 11:30:50', 0, 'msmf'),
(196, 'Kenneth Barr', 'Kenneth.Barr@syngeneintl.com', '7829000392', 'Bengaluru', NULL, NULL, '2021-01-18 11:31:13', '2021-01-18 11:31:13', '2021-01-18 12:23:35', 0, 'msmf'),
(197, 'Vijay Chandru', 'chandru@strandls.com', '9845202820', 'Bengaluru', NULL, NULL, '2021-01-18 11:32:31', '2021-01-18 11:32:31', '2021-01-18 12:30:54', 0, 'msmf'),
(198, 'Jagjit Anthak', 'jagjit.anthak@thermofisher.com', '9004076190', 'Mumbai ', NULL, NULL, '2021-01-18 11:32:32', '2021-01-18 11:32:32', '2021-01-18 12:27:10', 0, 'msmf'),
(199, 'Nilesh', 'nwadhwa1@its.jnj.com', '914724', 'Singapore', NULL, NULL, '2021-01-18 11:32:46', '2021-01-18 11:32:46', '2021-01-18 12:30:43', 0, 'msmf'),
(200, 'Sushma', 'SUSHMA.HEGDE@BIOCON.COM', '9620307744', 'Bengaluru', NULL, NULL, '2021-01-18 11:33:20', '2021-01-18 11:33:20', '2021-01-18 16:57:22', 0, 'msmf'),
(201, 'Kapil Sood', 'kapil.sood@thermofisher.com', '9845222544', 'Bangalore', NULL, NULL, '2021-01-18 11:33:27', '2021-01-18 11:33:27', '2021-01-18 12:24:02', 0, 'msmf'),
(202, 'Manish Sanghai', 'manish.sanghai@thermofisher.com', '9920837965', 'Mumbai', NULL, NULL, '2021-01-18 11:33:29', '2021-01-18 11:33:29', '2021-01-18 11:49:29', 0, 'msmf'),
(203, 'Aruna Janakiram', 'Finance@immuneel.com', '9980024963', 'Bangalore', NULL, NULL, '2021-01-18 11:33:30', '2021-01-18 11:33:30', '2021-01-18 11:42:26', 0, 'msmf'),
(204, 'Rakesh Natarajan', 'rakesh.natarajan@thermofisher.com', '9535468523', 'Bangalore', NULL, NULL, '2021-01-18 11:34:16', '2021-01-18 11:34:16', '2021-01-18 11:46:17', 0, 'msmf'),
(205, 'Dr Manish Diwan', 'Sped.birac@gov.in', '09871290710', 'New Delhi', NULL, NULL, '2021-01-18 11:35:54', '2021-01-18 11:35:54', '2021-01-18 12:22:39', 0, 'msmf'),
(206, 'Arvind Sonawane', 'arvind.sonawane@thermofisher.com', '9987079183', 'Mumbai', NULL, NULL, '2021-01-18 11:36:28', '2021-01-18 11:36:28', '2021-01-18 12:47:55', 0, 'msmf'),
(207, 'Sukrita Sethi', 'sukrita.sethi@biocon.com', '9910112309', 'Delhi', NULL, NULL, '2021-01-18 11:40:20', '2021-01-18 11:40:20', '2021-01-18 12:23:20', 0, 'msmf'),
(208, 'Chandana Mukheree ', 'shibutulumuke@yahoo.in', '9810273094', 'Delhi', NULL, NULL, '2021-01-18 11:40:43', '2021-01-18 11:40:43', '2021-01-18 12:31:43', 0, 'msmf'),
(209, 'Naveen', 'Naveenkumar@epeoplebc.com', '9880040887', 'Bangalore', NULL, NULL, '2021-01-18 11:40:55', '2021-01-18 11:40:55', '2021-01-18 12:24:16', 0, 'msmf'),
(210, 'Riddhi', 'riddhima.jaitly@biocon.com', '9742107514', 'Bengaluru ', NULL, NULL, '2021-01-18 11:43:05', '2021-01-18 11:43:05', '2021-01-18 11:59:46', 0, 'msmf'),
(211, 'Smritie Sheth', 'smritie.sheth@symphonytech.com', '08147340320', 'Pune', NULL, NULL, '2021-01-18 11:43:47', '2021-01-18 11:43:47', '2021-01-18 12:39:17', 0, 'msmf'),
(212, 'Moitry Patnaik', 'moitry.guha@gmail.com', '09766832134', 'Navi Mumbai', NULL, NULL, '2021-01-18 11:44:18', '2021-01-18 11:44:18', '2021-01-18 12:47:18', 0, 'msmf'),
(213, 'Siddhartha Mukherjee', 'siddhartha.mukherjee@thermofisher.com', '9901985789', 'Thermo Fisher Mumbai', NULL, NULL, '2021-01-18 11:45:09', '2021-01-18 11:45:09', '2021-01-18 12:31:40', 0, 'msmf'),
(214, 'Tanay Waingankar', 'tanay.waingankar@thermofisher.com', '09967504561', 'Mumbai', NULL, NULL, '2021-01-18 11:47:33', '2021-01-18 11:47:33', '2021-01-18 13:14:34', 0, 'msmf'),
(215, 'Randhir Singh', 'randhir.singh@biocon.com', '09999659988', 'Bengaluru', NULL, NULL, '2021-01-18 11:49:57', '2021-01-18 11:49:57', '2021-01-18 12:10:50', 0, 'msmf'),
(216, 'Arkasubhra Ghosh', 'arkasubhra@narayananethralaya.com', '9591236580', 'Bangalore', NULL, NULL, '2021-01-18 11:55:20', '2021-01-18 11:55:20', '2021-01-18 11:59:50', 0, 'msmf'),
(217, 'vijay kumar', 'vijay.kumar@thermofisher.com', '9620025073', 'Mumbai', NULL, NULL, '2021-01-18 11:55:22', '2021-01-18 11:55:22', '2021-01-18 13:01:54', 0, 'msmf'),
(218, 'vasanthy R', 'vasanthy.r@thermofisher.com', '08879002432', 'Mumbai', NULL, NULL, '2021-01-18 12:10:30', '2021-01-18 12:10:30', '2021-01-18 12:31:01', 0, 'msmf'),
(219, 'Jagadeesh', 'jagadeeshreddy.m@thermofisher.com', '8754599247', 'Bangalore', NULL, NULL, '2021-01-18 12:13:39', '2021-01-18 12:13:39', '2021-01-18 12:14:31', 0, 'msmf'),
(220, 'manjit singh', 'manjit.singh@anzchem.com.au', '61408020390', 'australia', NULL, NULL, '2021-01-18 12:23:29', '2021-01-18 12:23:29', '2021-01-18 12:24:31', 0, 'msmf'),
(221, 'raghavendra Goud', 'raghavendra.goud@cytiva.com', '9959566619', 'hyderabad', NULL, NULL, '2021-01-18 12:36:44', '2021-01-18 12:36:44', '2021-01-18 15:08:15', 0, 'msmf'),
(222, 'juan uriach', 'juan.uriach@uriach.com', '00346703923', 'barcelona espaÃ±a', NULL, NULL, '2021-01-18 14:36:50', '2021-01-18 17:39:04', '2021-01-18 17:39:34', 0, 'msmf'),
(223, 'Likitha', 'likitha@coact.co.in', '8105278762', 'Bangalore', NULL, NULL, '2022-01-05 13:39:28', '2022-01-05 13:39:28', '2022-01-05 13:40:29', 1, 'msmf'),
(224, 'neeraj', 'neeraj@cooact.co.in', '9700467764', 'nmg', NULL, NULL, '2022-01-10 17:25:49', '2022-01-10 17:25:49', '2022-01-10 17:26:19', 1, 'msmf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
